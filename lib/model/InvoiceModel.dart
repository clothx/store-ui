import 'package:clothx_stores/services/RandomStringService.dart';

class Invoice {
  var soldClothes;
  String phoneNumber;
  String typePayment;

  Invoice(this.soldClothes, this.phoneNumber, this.typePayment);

  toJson() {
    return {
      'uid': randomAlpha(5),
      'soldClothes': soldClothes,
      'phoneNumber': phoneNumber,
      'type': typePayment,
      'timeStamp': new DateTime.now()
    };
  }
}
