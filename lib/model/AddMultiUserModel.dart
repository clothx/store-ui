import 'package:clothx_stores/model/AuthUserModel.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:clothx_stores/services/RandomStringService.dart';

class AddMultiUser {
  String name;
  String role;
  String phoneNo;
  Auth currentUser;

  AddMultiUser(this.name, this.role, this.phoneNo) {
    currentUser = MyHomeState.sharedData;
  }

  toJson() {
    return {
      "name": name,
      "role": role,
      "phoneNumber": phoneNo,
      "password": randomAlpha(5),
      "isEmployee": true.toString(),
      "currentRole": currentUser.role,
      "currentUserToken": currentUser.token
    };
  }
}
