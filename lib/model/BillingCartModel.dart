class BillingCart {
  var crn = [];
  var sizes = [];
  var quantity = [];
  var type = [];
  var picUrl = [];
  var title = [];
  var price = [];
  int totalQuantity;
  int totalPrice;

  BillingCart(this.crn, this.sizes, this.quantity, this.type, this.picUrl,
      this.title, this.price, this.totalPrice, this.totalQuantity);

  toJson() {
    return {
      'crn': crn,
      'size': sizes,
      'quantity': quantity,
      'type': type,
      'picUrl': picUrl,
      'title': title,
      'price': price,
      'totalPrice': totalPrice,
      'totalQuantity': totalQuantity
    };
  }
}
