class Auth {
  final String role;
  final String key;
  final String type;
  final String phoneNumber;
  final String sid;
  final String name;
  final String token;

  Auth(
      {this.role,
      this.key,
      this.type,
      this.sid,
      this.phoneNumber,
      this.name,
      this.token});

  factory Auth.fromJson(Map<String, dynamic> json) {
    return new Auth(
        role: json['role'],
        type: json['type'],
        phoneNumber: json['phoneNumber'],
        sid: json['sid'],
        name: json['name'],
        token: json['token']);
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["role"] = role;
    map["type"] = type;
    map["phoneNumber"] = phoneNumber;
    map["sid"] = sid;
    map["name"] = name;
    map["token"] = token;

    return map;
  }
}

class User {
  final String phoneNumber;
  final String password;
  final String sid;

  User(this.phoneNumber, this.password, this.sid);

  toJson() {
    return {"phoneNumber": phoneNumber, "password": password, "sid": sid};
  }
}
