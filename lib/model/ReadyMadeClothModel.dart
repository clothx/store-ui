import 'package:clothx_stores/pages/HomePage.dart';

class ReadyMade {
  String gender;
  String brand;
  String typeOfWear;
  Map<String, int> size = {};
  var picUrl = [];
  int price;
  String type = 'Ready Made';

  ReadyMade(this.brand, this.typeOfWear, this.gender, this.size, this.price,
      this.picUrl);

  toJson() {
    return {
      "crn": 0,
      "brand": brand,
      "typeOfWear": typeOfWear,
      "gender": gender,
      "size": size,
      "price": price,
      "type": type,
      "picUrl": picUrl,
      "timeStamp": new DateTime.now(),
      "createdBy": MyHomeState.sharedData.phoneNumber
    };
  }
}
