import 'package:clothx_stores/pages/pages.dart';
import 'package:clothx_stores/services/AuthService.dart';
import 'package:flutter/material.dart';


class Routes {
  Routes() {
    runApp(new MyApp());
  }
}

class MyApp extends StatefulWidget {
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> implements AuthStateListener {
  bool isLoggedIn;

  MyAppState() {
    wait();
  }

  wait() {
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  @override
  onAuthStateChanged(AuthState state) {
    if (state == AuthState.LOGGED_IN) {
      setState(() {
        isLoggedIn = true;
      });
    } else
      setState(() {
        isLoggedIn = false;
      });
  }

  @override
  void dispose() {
    super.dispose();
    AuthStateProvider().dispose(this);
  }

  @override
  Widget build(BuildContext context) {
    if (isLoggedIn != null) {
      return new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "Clothx Store",
        theme: new ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.blueGrey[800],
          accentColor: Colors.blueGrey[800],
        ),
        routes: <String, WidgetBuilder>{
          //pages
          "/": (BuildContext context) =>
          isLoggedIn == true ? new MyHome() : new FirstPage(),
          "/login": (BuildContext context) => new LoginPage(FirstPageState.sid),
          "/home": (BuildContext context) => new MyHome(),
          "/selling": (BuildContext context) => new SellingPage(),
          "/addclothes": (BuildContext context) => new AddClothPage(),
          "/first": (BuildContext context) => new FirstPage(),
          "/dashboard": (BuildContext context) => new DashboardPage(),
          "/usermanagement": (BuildContext context) => new UserManagementPage(),
          "/navigation": (BuildContext context) => new NavigationPage(),
          "/invoice": (BuildContext context) => new InvoicePage(),
          "/mystore": (BuildContext context) => new MyStorePage(),

        },
      );
    } else
      return new Container(
        color: Colors.blueGrey[800],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              "CLOTHX",
              style: new TextStyle(
                  fontSize: 40.0,
                  fontWeight: FontWeight.w900,
                  color: Colors.white,
                  letterSpacing: 5.0),
              textDirection: TextDirection.ltr,
            ),
            new SizedBox(
              height: 20.0,
            ),
            new CircularProgressIndicator()
          ],
        ),
      );
  }
}
