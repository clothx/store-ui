import 'dart:async';

import 'package:clothx_stores/model/models.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:clothx_stores/pages/SendCCPage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SellingPage extends StatefulWidget {
  @override
  SellingPageState createState() => SellingPageState();
}

class SellingPageState extends State<SellingPage> {
  Auth currentUser;
  BillingCart billingCart;
  var soldClothes = [];
  var index = [];
  var quantity = [];
  var type = [];
  var crn = [];
  var picUrl = [];
  var title = [];
  var price = [];
  var totalSinglePrice = [];
  int totalPrice = 0;
  int totalQuantity = 0;

  TextEditingController _getController = new TextEditingController();
  CollectionReference getCoRef;

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _getController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    currentUser = MyHomeState.sharedData;
    billingCart = BillingCart(
        [],
        [],
        [],
        [],
        [],
        [],
        [],
        0,
        0);
    final firestore = Firestore.instance;
    getCoRef = firestore.collection('stores/' + currentUser.sid + '/clothes');
  }

  getCloth() {
    String crnValue = _getController.text;
    getCoRef
        .where("crn", isEqualTo: int.parse(_getController.text))
        .limit(1)
        .getDocuments()
        .then((docs) {
      if (docs.documents.isEmpty) {
        return _errorDialog("There is no product with " + crnValue + " as CRN");
      }
      docs.documents.forEach((doc) {
        Map stock = doc.data['size'];

        if (doc.data.isNotEmpty && doc.data['type'] == "Ready Made") {
          int i;
          int a = 0;
          for (i = 0; i < stock.length; i++) {
            if (stock[i] != "") {
              a = a + 1;
            }
          }
          if (a == 0)
            return _errorDialog("Out of stock");
          else {
            soldClothes.add(doc.data);
            setState(() {
              makeShoppingList();
            });
          }
        } else if (doc.data.isNotEmpty && doc.data['type'] == "Material") {
          if (doc.data['stock'] != 0) {
            soldClothes.add(doc.data);
            setState(() {
              makeShoppingList();
            });
          } else
            return _errorDialog("Out of stock");
        }
      });
    });

    _getController.clear();
  }

  next() {
    billingCart = BillingCart(
        crn,
        index,
        quantity,
        type,
        picUrl,
        title,
        totalSinglePrice,
        totalPrice,
        totalQuantity);

    if (soldClothes.isNotEmpty) {
      int s = 0;
      index.forEach((f) {
        if (f != null) s = s + 1;
      });
      if (s == index.length) {
        print(billingCart.toJson());
        Navigator.push(
          context,
          new MaterialPageRoute(
            builder: (context) => new SendCCPage(billingCart.toJson()),
          ),
        );

    }
      else
        _errorDialog("Add Cloth Size");
    } else
      _errorDialog("Billing Cart is Empty");

  }

  Future<Null> _errorDialog(error) {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('$error'),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  totalPriceCal() {
    totalPrice = 0;
    totalQuantity = 0;
    for (int i = 0; i < soldClothes.length; i++) {
      totalPrice = totalPrice + totalSinglePrice[i];
      totalQuantity = totalQuantity + quantity[i];
    }
    setState(() {
      totalPrice;
      totalQuantity;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        elevation: 10.0,
        backgroundColor: Colors.white,
        title: new TextField(
          controller: _getController,
          keyboardType: TextInputType.number,
          decoration: new InputDecoration(
              hintText: 'Enter cloth reference number',
              hintStyle: new TextStyle(color: Colors.grey, fontSize: 16.0),
              border: InputBorder.none),
          style: new TextStyle(
            color: Colors.black,
          ),
          autofocus: true,
        ),
        iconTheme: new IconThemeData(color: Colors.blueGrey[800]),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.add_circle),
            onPressed: () {
              getCloth();
            },
            color: Colors.teal,
          )
        ],
      ),
      body: new Container(
        color: Colors.white,
        child: new ListView(children: makeShoppingList()),
      ),
      floatingActionButton: new FloatingActionButton.extended(
        onPressed: () {
          next();
        },
        icon: new Icon(Icons.navigate_next),
        label: new Text(
          'Next',
          style: new TextStyle(letterSpacing: 2.0),
        ),
        backgroundColor: Colors.blue,
      ),
    );
  }

  // template ----------------------------------------------------------------------------------------------------------------------------------
  List<Widget> makeShoppingList() {
    List<Widget> list = new List<Widget>();

    price.length = soldClothes.length;
    crn.length = soldClothes.length;
    picUrl.length = soldClothes.length;
    title.length = soldClothes.length;
    type.length = soldClothes.length;
    quantity.length = soldClothes.length;
    index.length = soldClothes.length;
    totalSinglePrice.length = soldClothes.length;

    for (int i = 0; i < soldClothes.length; i++) {
      var item = soldClothes[i];
      var pic = item['picUrl'];
      var size = item['size'];
// data allocation
      if (soldClothes.isNotEmpty) {
        price[i] = item['price'];
        crn[i] = item['crn'];
        picUrl[i] = pic[0];
        title[i] =
            item['brand'] + " " + item['gender'] + " " + item['typeOfWear'];
        type[i] = item['type'];
      }

//text field data manipulation

      if (quantity[i] == null) {
        setState(() {
          quantity[i] = 1;
          totalSinglePrice[i] = price[i] * 1;
          totalPriceCal();
        });
      } else {
        quantity[i] = quantity[i];
      }

      _onQuantityChange() {
        if (quantity[i] != 0) {
          setState(() {
            totalSinglePrice[i] = price[i] * quantity[i];
          });
          totalPriceCal();
        } else {
          setState(() {
            totalSinglePrice[i] = 0;
          });

          totalPriceCal();
        }
      }

      list.add(
        new GestureDetector(
          onLongPress: () {
            soldClothes.removeAt(i);
            quantity.removeAt(i);
            index.removeAt(i);
            totalSinglePrice.removeAt(i);
            setState(() {
              makeShoppingList();
            });
          },
          child: new Card(
            margin: const EdgeInsets.only(bottom: 12.0, right: 2.0, left: 2.0),
            elevation: 5.0,
            child: new Column(
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Container(
                        margin: const EdgeInsets.only(right: 20.0),
                        child: new Image.network(
                          pic[0],
                          fit: BoxFit.fill,
                          width: 110.0,
                          height: 110.0,
                        )),
                    new Flexible(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                              item['brand'] +
                                  " " +
                                  item['gender'] +
                                  " " +
                                  item['typeOfWear'],
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                              )),
                          new Container(
                            margin: const EdgeInsets.only(top: 10.0),
                            child: new Chip(label: new Text(item['type'])),
                          )
                        ],
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        new IconButton(
                            icon: Icon(
                              Icons.keyboard_arrow_up, color: Colors.teal,),
                            onPressed: () {
                              quantity[i] = quantity[i] + 1;
                              _onQuantityChange();
                            }
                        ),
                        new Container(
                          width: 50.0,
                          height: 50.0,
                          child: new Card(
                              elevation: 5.0,
                              child: Center(
                                child: new Text(quantity[i].toString(),
                                  style: new TextStyle(
                                      fontSize: 15.0
                                  ),
                                ),
                              )
                          ),
                        ),
                        new IconButton(
                            icon: Icon(
                              Icons.keyboard_arrow_down, color: Colors.teal,),
                            onPressed: () {
                              if (quantity[i] != 1) {
                                quantity[i] = quantity[i] - 1;
                              }
                              _onQuantityChange();
                            }),
                      ],
                    ),
                    new Container(
                      margin: const EdgeInsets.only(right: 10.0),
                      child: new Text(
                        totalSinglePrice[i].toString() + ' INR ',
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
                new Container(
                  color: Colors.blueGrey[800],
                  child: new Container(
                    margin: const EdgeInsets.all(5.0),
                    child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: buildActionChips(size, i)),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    }

//    print("$type $crn $picUrl $title $price $total");
    return list;
  }

  void changeColor(key, ind) {
    setState(() {
      index[ind] = key;

      print(index);
    });
  }

  List<Widget> buildActionChips(data, ind) {
    List<Widget> list = new List<Widget>();
    if (data != null) {
      Map stock = {};
      stock = data;
      stock.forEach((key, v) {
        if (v != 0) {
          list.add(new Container(
            margin: const EdgeInsets.only(left: 10.0),
            child: new ActionChip(
              label: new Text(
                key,
                style: new TextStyle(
                  color: index[ind] == key ? Colors.white : Colors.black,
                ),
              ),
              onPressed: () {
                changeColor(key, ind);
              },
              backgroundColor:
              index[ind] == key ? Colors.blue[400] : Colors.white,
            ),
          ));
        }
      }); 
    }

    return list;
  }
}
