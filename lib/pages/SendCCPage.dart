import 'package:clothx_stores/model/models.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:clothx_stores/services/ValidationService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SendCCPage extends StatefulWidget {
  final data;

  SendCCPage(this.data);

  @override
  SendCCPageState createState() => SendCCPageState(data);
}

class SendCCPageState extends State<SendCCPage> {
  Auth currentUser;
  int _typeOfPayment = 0;
  var types = ["Cash", "Card", "Both"];
  Validations _validations = new Validations();
  Invoice invoice;
  var soldClothes;
  CollectionReference setCoRef;
  CollectionReference updateRef;
  SendCCPageState(this.soldClothes);

  TextEditingController _setController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    currentUser = MyHomeState.sharedData;
    invoice = Invoice(soldClothes, "", types[_typeOfPayment]);
    final firestore = Firestore.instance;
    setCoRef = firestore.collection('stores/' + currentUser.sid + '/invoice');
    updateRef = firestore.collection('stores');
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _setController.dispose();
    super.dispose();
  }

  setInvoice() {
    invoice.phoneNumber = _setController.text;
    setCoRef.add(invoice.toJson()).whenComplete(() {
      DocumentReference documentReference = updateRef.document(currentUser.sid);
      documentReference.get().then((doc) {
        if (doc.data['frequencyPoint'] != 0) {
          var latestPoint = (new DateTime.now().millisecondsSinceEpoch.toInt() -
              doc.data['frequencyPoint']) / 2;
          documentReference.updateData({"frequencyPoint": latestPoint});
        }
        else {
          documentReference.updateData({
            "frequencyPoint": new DateTime.now().millisecondsSinceEpoch.toInt()
          });
        }
      });
    });
    Navigator.of(context).pop();
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.blueGrey[800]),
        elevation: 0.0,
      ),
      body: new Container(
        color: Colors.white,
        child: new Center(
          child: new SingleChildScrollView(
            child: new Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Center(
                child: new Container(
                  margin: const EdgeInsets.all(10.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.all(10.0),
                            child: new Text(
                              'Total x' +
                                  soldClothes['totalQuantity'].toString(),
                              style: new TextStyle(
                                  fontSize: 25.0, fontWeight: FontWeight.w900),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10.0),
                            child: new Text(
                              soldClothes['totalPrice'].toString() + ' INR',
                              style: new TextStyle(
                                  fontSize: 25.0, fontWeight: FontWeight.w900),
                            ),
                          ),
                        ],
                      ),
                      new Card(
                        elevation: 5.0,
                        child: new Container(
                          margin: const EdgeInsets.all(10.0),
                          child: new TextField(
                            decoration: new InputDecoration(
                                hintText: 'Phone Number',
                                border: InputBorder.none),
                            keyboardType: TextInputType.phone,
                            controller: _setController,
                          ),
                        ),
                      ),
                      new SizedBox(
                        height: 25.0,
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: makeRadios(),
                      ),
                      new SizedBox(
                        height: 25.0,
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new FloatingActionButton(
                            child: new Icon(Icons.send),
                            onPressed: () {
                              setInvoice();
                            },
                            backgroundColor: Colors.blue,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> makeRadios() {
    List<Widget> list = new List<Widget>();

    for (int i = 0; i < 3; i++) {
      list.add(
        new Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              new Text(types[i]),
              new Radio(
                  value: i,
                  groupValue: _typeOfPayment,
                  onChanged: (int value) {
                    onChanged(value);
                  }),
            ],
          ),
        ),
      );
    }
    return list;
  }

  void onChanged(int value) {
    setState(() {
      _typeOfPayment = value;
    });

    invoice.typePayment = types[_typeOfPayment];
  }
}

