import 'package:clothx_stores/widgets/MaterialClothFormWidget.dart';
import 'package:clothx_stores/widgets/ReadyClothFormWidget.dart';
import 'package:flutter/material.dart';

class AddClothPage extends StatefulWidget {
  @override
  AddClothPageState createState() => AddClothPageState();
}

class AddClothPageState extends State<AddClothPage> {
  ReadyClothFormWidgetState ready;

  int _type = 0;

  var typeTile = ['Ready Made', 'Material'];

  void onChangedType(int value) {
    setState(() {
      _type = value;
      materialStock();
    });
  }

  List<Widget> makeTypeRadio() {
    List<Widget> list = new List<Widget>();

    for (int i = 0; i < 2; i++) {
      list.add(
        new Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              new Text(typeTile[i]),
              new Radio(
                groupValue: _type,
                value: i,
                onChanged: (int value) {
                  onChangedType(value);
                },
              ),
            ],
          ),
        ),
      );
    }
    return list;
  }

  List<Widget> materialStock() {
    List<Widget> materialList = new List<Widget>();
    List<Widget> readyMadeList = new List<Widget>();

    materialList.add(MaterialClothFormWidget());

    readyMadeList.add(ReadyClothFormWidget());

    if (_type == 0)
      return readyMadeList;
    else if (_type == 1) return materialList;

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blueGrey[800],
        elevation: 0.0,
        title: new Text(
          'Add Clothes',
          style: new TextStyle(letterSpacing: 2.0, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
      ),
      body: new Container(
        color: Colors.blueGrey[800],
        child: new Padding(
          padding: const EdgeInsets.only(left: 5.0, right: 5.0),
          child: new ListView(
            children: <Widget>[
//              new Card(
//                child: new Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                  children: makeTypeRadio(),
//                ),
//              ),
              new Column(
                children: <Widget>[ReadyClothFormWidget()],
              ),
              new SizedBox(
                height: 100.0,
              )
            ],
          ),
        ),
      ),
    );
  }
}
