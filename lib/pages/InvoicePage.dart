import 'package:clothx_stores/model/models.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:clothx_stores/widgets/FullScreenInvoiceViewWidget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
class InvoicePage extends StatefulWidget {
  @override
  InvoicePageState createState() => InvoicePageState();
}

class InvoicePageState extends State<InvoicePage> {
  CollectionReference coRef;
  Auth currentUser;

  @override
  void initState() {
    super.initState();
    currentUser = MyHomeState.sharedData;
    final firestore = Firestore.instance;
    coRef = firestore.collection('stores/' + currentUser.sid + '/invoice');
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Invoice',
          style: new TextStyle(letterSpacing: 2.0),
        ),
        centerTitle: true,
      ),
      body: new StreamBuilder(
          stream: coRef.snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return new Center(child: new CircularProgressIndicator());
            return new Container(
              child: new ListView.builder(
                itemCount: snapshot.data.documents.length,
                padding: const EdgeInsets.only(top: 10.0),
                itemExtent: 55.0,
                itemBuilder: (context, index) =>
                    _buildListItem(context, snapshot.data.documents[index]),
              ),
            );
          }),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    return new ListTile(
      key: new ValueKey(document.documentID),
      onTap: () {
        _viewInvoiceFSDialog(document);
      },
      title: new Text(document['phoneNumber']),
      subtitle: new Text(document['timeStamp'].toString()),
      trailing: new Text(
        document['uid'],
        style: new TextStyle(color: Colors.teal),
      ),
      isThreeLine: true,
    );
  }

  void _viewInvoiceFSDialog(doc) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new FullScreenInvoiceViewWidget(doc);
        },
        fullscreenDialog: true));
  }
}
