import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class DashboardPage extends StatefulWidget {
  @override
  DashboardPageState createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    var data = [
      new ClicksPerYear('6/6', 40, Colors.blue),
      new ClicksPerYear('7/6', 42, Colors.blue),
      new ClicksPerYear('8/6', 47, Colors.blue),
      new ClicksPerYear('Yesterday', 42, Colors.blue),
      new ClicksPerYear('Today', 47, Colors.blue),
    ];

    var series = [
      new charts.Series(
        id: 'Clicks',
        domainFn: (ClicksPerYear clickData, _) => clickData.year,
        measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
        colorFn: (ClicksPerYear clickData, _) => clickData.color,
        data: data,
      ),
    ];

    var chart = new charts.BarChart(
      series,
      animate: true,
    );
    var chartWidget = new Padding(
      padding: new EdgeInsets.all(32.0),
      child: new SizedBox(
        height: 200.0,
        child: chart,
      ),
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          'Dashboard',
          style: new TextStyle(
              letterSpacing: 2.0,
              fontWeight: FontWeight.w700,
              color: Colors.blueGrey[800]),
        ),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.white,
        iconTheme: new IconThemeData(color: Colors.blueGrey[800]),
      ),
      body: Container(
        color: Colors.white,
        child: new ListView(
          children: <Widget>[
            chartWidget,
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                elevation: 10.0,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      child: new Column(
                        children: <Widget>[
                          new Text('Total Products'),
                          new Text(
                            '5000',
                            style: new TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.w900),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      child: new Column(
                        children: <Widget>[
                          new Text('Total Sales'),
                          new Text(
                            '500',
                            style: new TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.w900),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(15.0),
                      child: new Column(
                        children: <Widget>[
                          new Text('Rating'),
                          new Text(
                            'Good',
                            style: new TextStyle(
                                fontSize: 25.0, fontWeight: FontWeight.w900),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            new SizedBox(
              height: 12.0,
            ),
            new SizedBox(
              height: 300.0,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Container(
                    width: 300.0,
                    child: new Card(
                      color: Colors.blueGrey,
                      elevation: 10.0,
                    ),
                  ),
                  Container(
                    width: 300.0,
                    child: new Card(
                      color: Colors.blueGrey,
                      elevation: 10.0,
                    ),
                  ),
                  Container(
                    width: 300.0,
                    child: new Card(
                      color: Colors.blueGrey,
                      elevation: 10.0,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = new charts.Color(
      r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
