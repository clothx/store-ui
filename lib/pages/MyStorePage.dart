import 'package:clothx_stores/model/AuthUserModel.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:clothx_stores/widgets/FullDetailClothWidget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class MyStorePage extends StatefulWidget {
  @override
  MyStorePageState createState() => MyStorePageState();
}

class MyStorePageState extends State<MyStorePage> {
  CollectionReference coRef;
  Auth currentUser;

  MyStorePageState() {
    currentUser = MyHomeState.sharedData;
  }

  @override
  void initState() {
    super.initState();
    final firestore = Firestore.instance;
    coRef = firestore.collection('stores/' + currentUser.sid + '/clothes');
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          'My Store',
          style: new TextStyle(letterSpacing: 2.0),
        ),
        centerTitle: true,
      ),
      body: new StreamBuilder(
          stream: coRef.snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return new Center(child: new CircularProgressIndicator());

            return new GridView.builder(
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 0.5,
                mainAxisSpacing: 0.5,
                crossAxisCount: 2,
                childAspectRatio: 0.5,
              ),
              scrollDirection: Axis.vertical,
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, index) =>
                  _buildGridItem(context, snapshot.data.documents[index]),
            );
          }),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, "/addclothes");
        },
        child: new Icon(Icons.add),
      ),
    );
  }

  Widget _buildGridItem(BuildContext context, DocumentSnapshot document) {
    var picUrls = [];
    picUrls = document['picUrl'];
    Size screenSize = MediaQuery
        .of(context)
        .size;
    return new GestureDetector(
      onTap: () {
        _openFullDialogClothDialog(document);
      },
      child: new Card(
          key: new ValueKey(document.documentID),
          child: new Stack(
            children: <Widget>[
              new Center(child: new CircularProgressIndicator()),
              new Positioned(
                top: 0.0,
                bottom: 0.0,
                left: screenSize.width * -0.5,
                right: screenSize.width * -0.5,
                child: new FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: picUrls[0],
                ),
              ),
              new Positioned(
                right: 0.0,
                child: new Card(
                    color: Colors.blue,
                    shape: new CircleBorder(),
                    child: new Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: new Text(
                        document['crn'].toString(),
                        style: new TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    )),
              ),
              new Positioned(
                  bottom: 0.0,
                  left: screenSize.width * -0.01,
                  child: new Container(
                    width: screenSize.width * 0.5,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Card(
                          shape: new CircleBorder(),
                          child: new IconButton(
                            icon: new Icon(Icons.delete),
                            onPressed: () {
                              coRef.document(document.documentID).delete();
                            },
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ))
            ],
          )),
    );
  }

  void _openFullDialogClothDialog(doc) {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new FullDetailClothWidget(doc);
        },
        fullscreenDialog: true));
  }
}
