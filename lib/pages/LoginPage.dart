import 'dart:convert';

import 'package:clothx_stores/model/AuthUserModel.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:clothx_stores/services/AuthService.dart';
import 'package:clothx_stores/services/DatabaseHelperService.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class LoginPage extends StatefulWidget {
  final sid;

  LoginPage(this.sid);

  @override
  LoginPageState createState() => LoginPageState(sid);
}

class LoginPageState extends State<LoginPage> implements AuthStateListener {
  var sid;
  ScrollController scrollController = new ScrollController();

  TextEditingController _phoneNumber = new TextEditingController();
  TextEditingController _password = new TextEditingController();
  String _info;
  BuildContext _ctx;
  String _server = "https://us-central1-clothxnet.cloudfunctions.net";

  LoginPageState(this.sid) {
    var authStateProvider = new AuthStateProvider();
    authStateProvider.subscribe(this);
  }

  @override
  void initState() {
    super.initState();
    _info = "";
  }

  @override
  void dispose() {
    super.dispose();
    AuthStateProvider().dispose(this);
  }

  @override
  onAuthStateChanged(AuthState state) {
    if (state == AuthState.LOGGED_IN) {
      Navigator.pushReplacement(
          _ctx,
          new MaterialPageRoute(
              builder: (BuildContext context) => new MyHome()));
    }
  }

  login() async {
    setState(() {
      _info = "checking..";
    });
    if (_phoneNumber.text.isNotEmpty && _password.text.isNotEmpty) {
      String _loginUrl = _server + "/login/";
      User user = new User(_phoneNumber.text, _password.text, sid);
      final response = await http.post(_loginUrl, headers: user.toJson());
      final responseJson = json.decode(response.body);
      print(responseJson);
      if (responseJson['isError'] == true) {
        setState(() {
          _info = responseJson['error'];
        });
      } else if (responseJson['isError'] == false) {
        Auth user = new Auth.fromJson(responseJson);
        var db = new DatabaseHelper();
        await db.saveUser(user);
        var authStateProvider = new AuthStateProvider();
        authStateProvider.notify(AuthState.LOGGED_IN);
      }
    } else
      setState(() {
        _info = "phone number or password is missing";
      });
  }

  @override
  Widget build(BuildContext context) {
    _ctx = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      body: new SingleChildScrollView(
        controller: scrollController,
        child: new Column(
          children: <Widget>[
            new Container(
                width: screenSize.width,
                height: screenSize.height,
                color: Colors.white,
                child: new Center(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text('CLOTHX' + sid.toString(),
                          style: new TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 30.0,
                            letterSpacing: 5.0,
                            color: Colors.blueGrey[800],
                          )),
                      new SizedBox(
                        height: screenSize.height * 0.05,
                      ),
                      new Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: new Card(
                          elevation: 5.0,
                          color: Colors.white,
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: new TextField(
                              controller: _phoneNumber,
                              decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  labelText: 'Phone Number',
                                  prefixText: '+91',
                                  prefixStyle: new TextStyle(
                                      fontSize: 20.0, color: Colors.black),
                                  labelStyle: new TextStyle(
                                    fontSize: 20.0,
                                  )),
                              style: new TextStyle(
                                  fontSize: 20.0, color: Colors.black),
                              keyboardType: TextInputType.phone,
                            ),
                          ),
                        ),
                      ),
                      new Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: new Card(
                          elevation: 5.0,
                          color: Colors.white,
                          child: new Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: new TextField(
                              controller: _password,
                              decoration: new InputDecoration(
                                  border: InputBorder.none,
                                  labelText: 'Password',
                                  labelStyle: new TextStyle(
                                      fontSize: 20.0, color: Colors.grey)),
                              style: new TextStyle(
                                  fontSize: 20.0, color: Colors.black),
                              keyboardType: TextInputType.text,
                              obscureText: true,
                            ),
                          ),
                        ),
                      ),
                      _info.isNotEmpty
                          ? new Text(
                        _info,
                        style: new TextStyle(
                            fontSize: 12.0,
                            color: Colors.blueGrey[800],
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      )
                          : new Text(""),
                      new SizedBox(
                        height: screenSize.height * 0.05,
                      ),
                      new Card(
                          elevation: 5.0,
                          color: Colors.white,
                          child: new IconButton(
                            tooltip: "Next",
                            icon: new Icon(Icons.navigate_next),
                            onPressed: () {
                              login();
                            },
                            color: Colors.blueGrey[800],
                          ),
                          shape: const CircleBorder()),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
