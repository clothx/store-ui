import 'package:clothx_stores/model/AuthUserModel.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:clothx_stores/widgets/AddUserFSDialogWidget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserManagementPage extends StatefulWidget {
  @override
  UserManagementPageState createState() => UserManagementPageState();
}

class UserManagementPageState extends State<UserManagementPage> {
  CollectionReference coRef;
  Auth user;

  @override
  void initState() {
    super.initState();
    user = MyHomeState.sharedData;
    final firestore = Firestore.instance;
    coRef = firestore.collection('stores/' + user.sid + '/employees');
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          'User Management',
          style: new TextStyle(letterSpacing: 2.0),
        ),
        centerTitle: true,
      ),
      body: new StreamBuilder(
          stream: coRef.where('isEmployee', isEqualTo: true).snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData)
              return new Center(child: new CircularProgressIndicator());
            return new Container(
              child: new ListView.builder(
                itemCount: snapshot.data.documents.length,
                padding: const EdgeInsets.only(top: 10.0),
                itemExtent: 55.0,
                itemBuilder: (context, index) =>
                    _buildListItem(context, snapshot.data.documents[index]),
              ),
            );
          }),
      floatingActionButton: new FloatingActionButton(
        onPressed: () {
          _openAddUserFSDialog();
        },
        child: new Icon(Icons.person_add),
      ),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    return new ListTile(
      key: new ValueKey(document.documentID),
      title: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          new Expanded(
            child: new Text(
              document['name'],
              style: new TextStyle(fontSize: 20.0),
            ),
          ),
          new Text(
            document['role'].toString(),
          ),
        ],
      ),
      trailing: new IconButton(
          icon: Icon(
            Icons.delete,
            color: Colors.red,
          ),
          onPressed: () => coRef.document(document.documentID).delete()),
    );
  }

  void _openAddUserFSDialog() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new AddUserFSDialogWidget();
        },
        fullscreenDialog: true));
  }
}
