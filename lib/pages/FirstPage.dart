import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FirstPage extends StatefulWidget {
  @override
  FirstPageState createState() => FirstPageState();
}

class FirstPageState extends State<FirstPage> {
  ScrollController scrollController = new ScrollController();
  TextEditingController _sidController = new TextEditingController();
  static String sid;
  String _info;
  CollectionReference coRef;

  @override
  void initState() {
    super.initState();
    _info = "";
    coRef = Firestore.instance.collection('stores');
  }

  checkSid() {
    setState(() {
      _info = "checking..";
    });
    sid = _sidController.text;
    if (sid.isNotEmpty) {
      coRef.document(sid).get().then((sid) {
        if (sid.exists) {
          setState(() {
            _info = "";
          });
          Navigator.pushReplacementNamed(context, "/login");
        } else {
          setState(() {
            _info = "your store id doesn't exit";
          });
        }
      });
    } else {
      setState(() {
        _info = "please type your store id";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      body: new SingleChildScrollView(
        controller: scrollController,
        child: new Column(
          children: <Widget>[
            new Container(
                width: screenSize.width,
                height: screenSize.height,
                color: Colors.blueGrey[800],
                child: new Center(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text('CLOTHX STORES',
                          style: new TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 30.0,
                            letterSpacing: 4.0,
                            color: Colors.white,
                          )),
                      new SizedBox(
                        height: screenSize.height * 0.05,
                      ),
                      new Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: new Card(
                          elevation: 5.0,
                          color: Colors.blueGrey,
                          child: new Container(
                            margin: const EdgeInsets.all(8.0),
                            child: new Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: new TextField(
                                controller: _sidController,
                                decoration: new InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'SID',
                                    hintStyle: new TextStyle(
                                        fontSize: 20.0, color: Colors.white)),
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 20.0),
                                keyboardType: TextInputType.number,
                              ),
                            ),
                          ),
                        ),
                      ),
                      _info.isNotEmpty
                          ? new Text(
                        _info,
                        style: new TextStyle(
                            fontSize: 12.0,
                            color: Colors.white,
                            letterSpacing: 1.0,
                            fontWeight: FontWeight.bold),
                      )
                          : new Text(""),
                      new SizedBox(
                        height: screenSize.height * 0.05,
                      ),
                      new Card(
                          elevation: 5.0,
                          color: Colors.blueGrey,
                          child: new IconButton(
                            icon: new Icon(Icons.navigate_next),
                            onPressed: () {
                              checkSid();
                            },
                            color: Colors.white,
                          ),
                          shape: const CircleBorder())
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
