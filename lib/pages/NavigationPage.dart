import 'dart:async';

import 'package:clothx_stores/services/DatabaseHelperService.dart';
import 'package:clothx_stores/widgets/DrawerListWidget.dart';
import 'package:flutter/material.dart';

class NavigationPage extends StatefulWidget {
  NavigationPageState createState() => NavigationPageState();
}

class NavigationPageState extends State<NavigationPage> {
  BuildContext _cxt;

  logOut() async {
    var db = new DatabaseHelper();
    await db.deleteUsers();
    Navigator.of(context).pop();
    Navigator.pushReplacementNamed(_cxt, "/first");
  }

  Future<Null> logOutDialog() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Are you sure?'),
          actions: <Widget>[
            new FlatButton(
              child: new Text('LOGOUT'),
              onPressed: () {
                Navigator.of(context).pop();
                logOut();
              },
            ),
            new FlatButton(
              child: new Text('STAY'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _cxt = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.exit_to_app),
              onPressed: () {
                logOutDialog();
              })
        ],
      ),
      body: new SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: new Container(
          width: screenSize.width,
          height: screenSize.height,
          child: DrawerListWidget(),
        ),
      ),
    );
  }
}
