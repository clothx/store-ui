import 'package:clothx_stores/model/AuthUserModel.dart';
import 'package:clothx_stores/services/DatabaseHelperService.dart';
import 'package:flutter/material.dart';

//9843158807
//9047365525
class MyHome extends StatefulWidget {
  @override
  MyHomeState createState() => MyHomeState();
}

class MyHomeState extends State<MyHome> {
  static Auth sharedData;

  MyHomeState() {
    getCurrentUser();
  }

  getCurrentUser() async {
    var db = new DatabaseHelper();
    await db.getUser().then((user) {
      setState(() {
        sharedData = user;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            'CLOTHX STORES',
            style: new TextStyle(
              color: Colors.blueGrey[800],
              letterSpacing: 4.0,
              fontWeight: FontWeight.w900,
            ),
          ),
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: new Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.blue,
          child: new Column(
            children: <Widget>[
              new Expanded(
                child: new MaterialButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/addclothes");
                  },
                  color: Colors.white,
                  child: new Center(
                    child: new Text(
                      'Add',
                      style: new TextStyle(
                          fontSize: 35.0,
                          fontWeight: FontWeight.w900,
                          letterSpacing: 2.0,
                          color: Colors.blueGrey[800]),
                    ),
                  ),
                ),
              ),
              new Expanded(
                child: new MaterialButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/mystore");
                  },
                  color: Colors.blueGrey[800],
                  child: new Center(
                    child: Text(
                      'Store',
                      style: new TextStyle(
                          fontSize: 35.0,
                          fontWeight: FontWeight.w900,
                          letterSpacing: 2.0,
                          color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        bottomNavigationBar: new PreferredSize(
          child: new BottomAppBar(
            color: Colors.blueGrey[800],
            elevation: 0.0,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new IconButton(
                  icon: new Icon(Icons.menu),
                  onPressed: () {
                    Navigator.pushNamed(context, "/navigation");
                  },
                  color: Colors.white,
                ),
//                  currentUser.role == 'owner' ? new IconButton(
//                    icon: new Icon(Icons.people),
//                    onPressed: () {
//                      Navigator.pushNamed(context, "/usermanagement");
//                    },
//                    color: Colors.white,
//                  ) : new Opacity(opacity: 0.0)
              ],
            ),
          ),
          preferredSize: const Size.fromHeight(40.0),
        ));
  }
}
