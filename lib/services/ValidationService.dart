class Validations {
  String validateName(String value) {
    if (value.isEmpty) return 'Name is required.';
    final RegExp nameExp = new RegExp(r'^[A-za-z ]+$');
    if (!nameExp.hasMatch(value))
      return 'Please enter only alphabetical characters.';
    return null;
  }

  String validateEmail(String value) {
    if (value.isEmpty) return 'Email is required.';
    final RegExp nameExp = new RegExp(r'^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$');
    if (!nameExp.hasMatch(value)) return 'Invalid email address';
    return null;
  }

  String validatePassword(String value) {
    if (value.isEmpty) return 'Please choose a password.';
    return null;
  }

  String validateText(String value) {
    if (value.isEmpty) return 'Name is required.';

    return null;
  }

  String validateStock(String value) {
    final RegExp nameExp = new RegExp(r'^[0-9]\d*(\.\d+)?$');
    if (value.isNotEmpty) {
      if (!nameExp.hasMatch(value)) return 'Please enter only numbers.';
    }
    return null;
  }

  String validatePrice(String value) {
    if (value.isEmpty) return 'Price is required.';
    final RegExp nameExp = new RegExp(r'^[0-9]\d*(\.\d+)?$');
    if (!nameExp.hasMatch(value)) return 'Please enter only numbers.';
    return null;
  }

  String validatePhone(String value) {
    if (value.isEmpty) return 'Phone Number is required.';
    final RegExp nameExp = new RegExp(r'^[0-9\+]{0,10}$');
    if (!nameExp.hasMatch(value)) return 'Please enter Phone number.';
    return null;
  }
}
