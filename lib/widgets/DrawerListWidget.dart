import 'package:clothx_stores/model/AuthUserModel.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DrawerListWidget extends StatefulWidget {
  DrawerListWidgetState createState() => DrawerListWidgetState();
}

class DrawerListWidgetState extends State<DrawerListWidget> {
  Auth currentUser;
  CollectionReference coRef;
  DrawerListWidgetState() {
    currentUser = MyHomeState.sharedData;
  }

  @override
  void initState() {
    super.initState();
    final firestore = Firestore.instance;
    coRef = firestore.collection('stores/' + currentUser.sid + '/clothes');
  }

  @override
  Widget build(BuildContext context) {
    return new ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        new UserAccountsDrawerHeader(
          accountName: new Text(
            currentUser.name.toUpperCase(),
            style: new TextStyle(
                fontSize: 25.0,
                fontWeight: FontWeight.w700,
                letterSpacing: 1.0),
          ),
          accountEmail: new Text(
            currentUser.phoneNumber,
            style: new TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.w500,
                letterSpacing: 0.5),
          ),
        ),

//        currentUser.role == 'owner'
//            ? new ListTile(
//                leading: new Icon(Icons.dashboard),
//                title: new Text('Dashboard'),
//                onTap: () {
//                  Navigator.pushNamed(context, "/dashboard");
//                },
//              )
//            : new Opacity(opacity: 0.0),

        currentUser.role == 'owner'
            ? new ListTile(
          leading: new Icon(Icons.group),
          title: new Text('User Management'),
          onTap: () {
            Navigator.pushNamed(context, "/usermanagement");
          },
        )
            : new Opacity(opacity: 0.0),

        currentUser.role == 'seller' || currentUser.role == 'owner' ?
        Column(
          children: <Widget>[
            new ListTile(
              leading: new Icon(Icons.add_shopping_cart),
              title: new Text('Sell'),
              onTap: () {
                Navigator.pushNamed(context, "/selling");
              },
            ),
            new ListTile(
              leading: new Icon(Icons.recent_actors),
              title: new Text('Invoice'),
              onTap: () {
                Navigator.pushNamed(context, "/invoice");
              },
            ),


          ],
        ) : new Opacity(opacity: 0.0),


        currentUser.role == 'data entry' || currentUser.role == 'supervisor'
            ? Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Container(
            height: 500.0,
            child: new ListView(
              children: <Widget>[
                new Divider(
                  color: Colors.black,
                ),
                new StreamBuilder(
                    stream: coRef
                        .where('createdBy',
                        isEqualTo: currentUser.phoneNumber)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData)
                        return new Center(
                            child: new CircularProgressIndicator());

                      return Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new Text(
                                'Status',
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,
                                    color: Colors.grey[800]),
                              ),
                              new Text(
                                snapshot.data.documents.length.toString(),
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20.0,
                                    color: Colors.grey[800]),
                              ),
                            ],
                          ),
                          new Container(
                            height: 400.0,
                            child: new ListView.builder(
                              itemCount: snapshot.data.documents.length,
                              padding: const EdgeInsets.only(top: 10.0),
                              itemExtent: 55.0,
                              itemBuilder: (context, index) =>
                                  _buildListItem(context,
                                      snapshot.data.documents[index]),
                            ),
                          ),
                        ],
                      );
                    })
              ],
            ),
          ),
        )
            : new Opacity(opacity: 0.0),

//        new ListTile(
//          leading: new Icon(Icons.fiber_smart_record),
//          title: new Text('Predictions'),
//          onTap: () {
//            // Update the state of the app
//            // ...
//          },
//        ),

//
//        new ListTile(
//          leading: new Icon(Icons.exit_to_app),
//          title: new Text('Sign Out'),
//          onTap: () {
//            // Update the state of the app
//            // ...
//          },
//        ),
      ],
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot document) {
    return new ListTile(
      key: new ValueKey(document.documentID),
      title: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          new Expanded(
            child: new Text(
              document['crn'].toString(),
              style: new TextStyle(fontSize: 20.0),
            ),
          ),
          new Text(
            document['timeStamp'].toString(),
          ),
        ],
      ),
    );
  }
}
