import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FullScreenInvoiceViewWidget extends StatefulWidget {
  final invoice;

  FullScreenInvoiceViewWidget(this.invoice);

  @override
  FullScreenInvoiceViewWidgetState createState() =>
      FullScreenInvoiceViewWidgetState(invoice);
}

class FullScreenInvoiceViewWidgetState
    extends State<FullScreenInvoiceViewWidget> {
  DocumentSnapshot invoice;
  var data;
  var soldClothes;
  var crn = [];
  var picUrl = [];
  var price = [];
  var quantity = [];
  var size = [];
  var title = [];
  var type = [];
  FullScreenInvoiceViewWidgetState(this.invoice);

  @override
  Widget build(BuildContext context) {
    data = invoice.data;
    soldClothes = data['soldClothes'];
    picUrl = soldClothes['picUrl'];
    crn = soldClothes['crn'];
    price = soldClothes['price'];
    quantity = soldClothes['quantity'];
    size = soldClothes['size'];
    title = soldClothes['title'];
    type = soldClothes['type'];

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(data['uid'].toString().toUpperCase()),
        actions: <Widget>[
          Container(
            margin: const EdgeInsets.only(right: 5.0),
            child: Center(
                child: new Text(
              'Total ' + 'x' + soldClothes['totalQuantity'].toString(),
              style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            )),
          ),
          Container(
            margin: const EdgeInsets.only(right: 5.0),
            child: Center(
                child: new Text(
              soldClothes['totalPrice'].toString() + " INR",
              style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
            )),
          )
        ],
      ),
      body: new Container(
        child: new ListView(
          scrollDirection: Axis.vertical,
          children: makeShoppingList(),
        ),
      ),
    );
  }

  List<Widget> makeShoppingList() {
    List<Widget> list = new List<Widget>();

    for (int i = 0; i < crn.length; i++) {
      list.add(
        new Card(
          margin: const EdgeInsets.only(bottom: 12.0, right: 2.0, left: 2.0),
          elevation: 5.0,
          child: new Column(
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container(
                      margin: const EdgeInsets.only(right: 20.0),
                      child: new Image.network(
                        picUrl[i],
                        fit: BoxFit.fill,
                        width: 110.0,
                        height: 110.0,
                      )),
                  new Flexible(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(title[i],
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                        new Container(
                          margin: const EdgeInsets.only(top: 10.0),
                          child: new Chip(label: new Text(type[i])),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    width: 50.0,
                    height: 50.0,
                    child: new Card(
                        elevation: 5.0,
                        child: new Center(
                          child: size[i] == null
                              ? new Text('${quantity[i]} m',
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold))
                              : new Text(
                            '${quantity[i]}' + "-" + size[i],
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                        )),
                  ),
                  new Container(
                    margin: const EdgeInsets.only(right: 10.0),
                    child: new Text(
                      '${price[i]} INR ',
                      style: new TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    return list;
  }
}
