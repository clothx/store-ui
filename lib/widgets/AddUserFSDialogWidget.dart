import 'dart:convert';

import 'package:clothx_stores/model/AddMultiUserModel.dart';
import 'package:clothx_stores/services/ValidationService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AddUserFSDialogWidget extends StatefulWidget {
  @override
  AddUserFSDialogWidgetState createState() => AddUserFSDialogWidgetState();
}

class AddUserFSDialogWidgetState extends State<AddUserFSDialogWidget> {
  AddMultiUser addUser;
  CollectionReference coRef;
  Validations _validations = new Validations();
  bool _autovalidate = false;
  String _info = "";

  @override
  void initState() {
    super.initState();

    addUser = AddMultiUser("", "", "");
  }

  var options = ['Data Entry', 'Seller', 'Supervisor'];
  int _isSelected = 0;
  final GlobalKey<FormState> _addUserFormKey = new GlobalKey<FormState>();

  void onChanged(int value) {
    setState(() {
      _isSelected = value;
    });
    print('value = $value');
  }

  List<Widget> makeRadios() {
    List<Widget> list = new List<Widget>();

    for (int i = 0; i < 3; i++) {
      list.add(
        new Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              new Text(options[i]),
              new Radio(
                  value: i,
                  groupValue: _isSelected,
                  onChanged: (int value) {
                    onChanged(value);
                  }),
            ],
          ),
        ),
      );
    }
    return list;
  }

  handleSave() {
    final FormState form = _addUserFormKey.currentState;
    if (!form.validate()) {
      _autovalidate = true;
    } else {
      addUser.role = options[_isSelected].toLowerCase();
      form.save();
      var data = addUser.toJson();
      print(data);
      sent(data);
      form.reset();
      Navigator.of(context).pop();
    }
  }

  sent(data) async {
    final response = await http.post(
        "https://us-central1-clothxnet.cloudfunctions.net/addemployee/",
        headers: data);
    final responseJson = json.decode(response.body);

    if (responseJson['isError'] == true) {
      return setState(() {
        _info = responseJson['msg'];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery
        .of(context)
        .size;
    return new Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        title: new Text(
          'Add new user',
          style: new TextStyle(letterSpacing: 2.0),
        ),
        centerTitle: true,
        actions: <Widget>[
          new FlatButton(
              onPressed: () => handleSave(),
              child: new Text(
                'Save',
                style: new TextStyle(color: Colors.white),
              ))
        ],
      ),
      body: new SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: new Container(
          height: screenSize.height,
          color: Colors.blueGrey[800],
          padding: const EdgeInsets.all(10.0),
          child: new Form(
              key: _addUserFormKey,
              autovalidate: _autovalidate,
              child: new Column(
                children: <Widget>[
                  new Card(
                    elevation: 10.0,
                    child: new Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: new TextFormField(
                        validator: _validations.validateName,
                        onSaved: (val) => addUser.name = val,
                        decoration: new InputDecoration(
                            labelText: 'Name', border: InputBorder.none),
                      ),
                    ),
                  ),
                  new Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: new Card(
                      elevation: 10.0,
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: makeRadios(),
                      ),
                    ),
                  ),
                  new Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child: new Card(
                      elevation: 10.0,
                      child: new Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: new TextFormField(
                          validator: _validations.validatePhone,
                          onSaved: (val) => addUser.phoneNo = val,
                          keyboardType: TextInputType.phone,
                          decoration: new InputDecoration(
                              prefixText: '+91',
                              labelText: 'Phone number',
                              border: InputBorder.none),
                        ),
                      ),
                    ),
                  ),
                  _info.isNotEmpty
                      ? new Text(
                    _info,
                    style: new TextStyle(
                        fontSize: 12.0,
                        color: Colors.white,
                        letterSpacing: 1.0,
                        fontWeight: FontWeight.bold),
                  )
                      : new Text(""),
                ],
              )),
        ),
      ),
    );
  }
}
