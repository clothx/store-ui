import 'dart:async';

import 'package:clothx_stores/services/ValidationService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class MaterialClothFormWidget extends StatefulWidget {
  @override
  MaterialClothFormWidgetState createState() => MaterialClothFormWidgetState();
}

class MaterialClothFormWidgetState extends State<MaterialClothFormWidget> {
  CollectionReference coRef;
  bool isUploading = false;
  MaterialCloth materialCloth;
  int _gender = 0;
  var picUrl = [];
  var picListLength = 0;
  var options = ['Men', 'Women', 'Boys', 'Girls'];
  Validations _validations = new Validations();
  bool _autovalidate = false;

  void onChanged(int value) {
    setState(() {
      _gender = value;
    });

    print(options[value]);
  }

  Future<Null> _errorDialog(error) {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('$error'),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();

    materialCloth = MaterialCloth(
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        []);
    final firestore = Firestore.instance;
    coRef = firestore.collection('clothes');
  }

  List<Widget> makeRadios() {
    List<Widget> list = new List<Widget>();

    for (int i = 0; i < 4; i++) {
      list.add(
        new Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              new Text(options[i]),
              new Radio(
                  value: i,
                  groupValue: _gender,
                  onChanged: (int value) {
                    onChanged(value);
                  }),
            ],
          ),
        ),
      );
    }
    return list;
  }

  final GlobalKey<FormState> _materialClothAddingFormKey =
  new GlobalKey<FormState>();

  void handleSave() {
    final FormState form = _materialClothAddingFormKey.currentState;

    if (!form.validate()) {
      _autovalidate = true;
    } else if (picUrl.isNotEmpty) {
      materialCloth.gender = options[_gender];
      materialCloth.picUrl = picUrl;
      form.save();

      form.reset();
      coRef.add(materialCloth.toJson());
      print('sucess');
      picUrl = [];
      setState(() {
        picListLength = picUrl.length;
      });
    } else
      _errorDialog('Atleast minimum a picture');
  }

  Future<Null> _handleAddPhoto() async {
    if (picUrl.length <= 3) {
      var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
      var random = new DateTime.now().millisecondsSinceEpoch;
      var ref = FirebaseStorage.instance
          .ref()
          .child('materials/material_$random.jpg');
      if (imageFile != null) {
        setState(() {
          isUploading = true;
        });
      }

//      var compressedImage = await compute(convert, imageFile);

      var uploadTask = ref.putFile(imageFile);
      var downloadUrl = (await uploadTask.future).downloadUrl;
      picUrl.add(downloadUrl.toString());
      setState(() {
        isUploading = false;
        picListLength = picUrl.length;
      });
    } else
      print('upload limit exceeded');
  }

//
//  convert(imageFile) async{
//    final tempDir = await getTemporaryDirectory();
//    final path = tempDir.path;
//    Im.Image image =
//        Im.decodeImage(imageFile.readAsBytesSync());
//    Im.Image smallerImage = Im.copyResize(
//        image, 500); // choose the size here, it will maintain aspect ratio
//    var random = new DateTime.now().millisecondsSinceEpoch;
//    var compressedImage = new Io.File('$path/img_$random.jpg')
//      ..writeAsBytesSync(Im.encodeJpg(smallerImage, quality: 85));
//    return compressedImage;
//  }

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new Form(
          key: _materialClothAddingFormKey,
          autovalidate: _autovalidate,
          child: new Column(children: <Widget>[
            new Padding(
              padding: const EdgeInsets.all(2.0),
              child: new Column(
                children: <Widget>[
                  new TextFormField(
                    validator: _validations.validateText,
                    onSaved: (val) => materialCloth.brand = val,
                    decoration: new InputDecoration(
                      border: InputBorder.none,
                      labelText: "Brand",
                      fillColor: Colors.white,
                      filled: true,
                    ),
                    style: new TextStyle(
                        color: Colors.blueGrey[800], fontSize: 16.0),
                  ),
                  new Divider(
                    color: Colors.black,
                  ),
                  new TextFormField(
                    validator: _validations.validateText,
                    onSaved: (val) => materialCloth.typeOfWear = val,
                    decoration: new InputDecoration(
                      border: InputBorder.none,
                      labelText: "Type of Cloth ",
                      fillColor: Colors.white,
                      filled: true,
                    ),
                    style: new TextStyle(
                        color: Colors.blueGrey[800], fontSize: 16.0),
                  ),
                  new Divider(
                    color: Colors.black,
                  ),
                  new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: makeRadios()),
                  new Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            new TextFormField(
              validator: _validations.validateText,
              onSaved: (val) => materialCloth.material = val,
              keyboardType: TextInputType.text,
              decoration: new InputDecoration(
                border: InputBorder.none,
                labelText: "Material Type",
                fillColor: Colors.white,
                filled: true,
              ),
              style: new TextStyle(color: Colors.blueGrey[800], fontSize: 16.0),
            ),
            new Divider(
              color: Colors.black,
            ),
            new TextFormField(
              validator: _validations.validateStock,
              onSaved: (val) => materialCloth.stock = val,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                border: InputBorder.none,
                labelText: "Stock in meter",
                fillColor: Colors.white,
                filled: true,
              ),
              style: new TextStyle(color: Colors.blueGrey[800], fontSize: 16.0),
            ),
            new Divider(
              color: Colors.black,
            ),
            new TextFormField(
              validator: _validations.validatePrice,
              onSaved: (val) => materialCloth.actualPrice = val,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                border: InputBorder.none,
                labelText: "Actual price",
                fillColor: Colors.white,
                filled: true,
              ),
              style: new TextStyle(color: Colors.blueGrey[800], fontSize: 16.0),
            ),
            new Divider(
              color: Colors.black,
            ),
            new TextFormField(
              validator: _validations.validatePrice,
              onSaved: (val) => materialCloth.sellingPrice = val,
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                border: InputBorder.none,
                labelText: "Selling price",
                fillColor: Colors.white,
                filled: true,
              ),
              style: new TextStyle(color: Colors.blueGrey[800], fontSize: 16.0),
            ),
            new Divider(
              color: Colors.black,
            ),
            new Container(
              margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Row(
                    children: <Widget>[
                      new Card(
                        elevation: 5.0,
                        margin: const EdgeInsets.all(10.0),
                        shape: const CircleBorder(),
                        child: new IconButton(
                            icon: new Icon(Icons.add_a_photo),
                            onPressed: () => _handleAddPhoto()),
                      ),
                      isUploading == true
                          ? Row(
                        children: <Widget>[
                          new Container(
                              width: 20.0,
                              height: 20.0,
                              child: new CircularProgressIndicator()),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: new Text("Uploading.."),
                          )
                        ],
                      )
                          : new Text('$picListLength pictures added')
                    ],
                  ),
                  new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new RaisedButton(
                      onPressed: () => handleSave(),
                      child: new Text('Save'),
                    ),
                  ),
                ],
              ),
            )
          ])),
    );
  }
}

class MaterialCloth {
  String gender;
  String brand;
  String typeOfWear;
  String material;
  String stock;
  var picUrl = [];
  String actualPrice;
  String sellingPrice;

  String type = 'Material';

  MaterialCloth(this.gender, this.brand, this.typeOfWear, this.material,
      this.stock, this.actualPrice, this.sellingPrice, this.picUrl);

  toJson() {
    return {
      "crn": '45',
      "gender": gender,
      "brand": brand,
      "typeOfWear": typeOfWear,
      "stock": stock,
      "actualPrice": actualPrice,
      "sellingPrice": sellingPrice,
      "type": type,
      "picUrl": picUrl,
      "timeStamp": new DateTime.now()
    };
  }
}
