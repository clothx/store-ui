import 'package:clothx_stores/model/models.dart';
import 'package:clothx_stores/pages/pages.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
class FullDetailClothWidget extends StatefulWidget {
  final cloth;

  FullDetailClothWidget(this.cloth);

  @override
  FullDetailClothWidgetState createState() => FullDetailClothWidgetState(cloth);
}

class FullDetailClothWidgetState extends State<FullDetailClothWidget> {
  DocumentSnapshot cloth;


  FullDetailClothWidgetState(this.cloth);

  CollectionReference updateRef;
  DocumentReference docRef;
  Auth currentUser;
  var data;
  var picUrl = [];
  var db;
  bool isChanged = false;
  @override
  void initState() {
    super.initState();
    currentUser = MyHomeState.sharedData;
    var firestore = Firestore.instance;
    updateRef = firestore.collection('stores');
    db = Firestore.instance;

  }

  List<Widget> getPics() {
    List<Widget> list = new List<Widget>();

    for (int i = 0; i < picUrl.length; i++) {
      list.add(
        new Container(
            width: 450.0,
            child: new Image.network(
              picUrl[i],
              fit: BoxFit.fitWidth,
            )),
      );
    }
    return list;
  }

  List<Widget> makeSize() {
    List<Widget> list = new List<Widget>();

    Map _size = data['size'];
    _size.forEach((key, value) {
      list.add(
        Column(
          children: <Widget>[
            new Container(
              width: 75.0,
              height: 50.0,
              child: new Card(
                elevation: 5.0,
                child: Center(
                  child: new Text(
                    key,
                    style: new TextStyle(
                      fontSize: 15.0,
                    ),
                  ),
                ),
              ),
            ),
            new Container(
              width: 75.0,
              height: 130.0,
              child: new Card(
                  elevation: 5.0,
                  child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          new IconButton(
                              icon: Icon(
                                Icons.add,
                                color: Colors.teal,
                              ),
                              onPressed: () {
                                increment(key, value);
                              }),
                          new Text(
                            value.toString(),
                            style: new TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.bold),
                          ),
                          new IconButton(
                              icon: Icon(
                                Icons.remove,
                                color: Colors.red,
                              ),
                              onPressed: () => decrement(key, value))
                        ],
                      ))),
            ),
          ],
        ),
      );
    });
    return list;
  }

  increment(key, value) {
    Map size = cloth.data['size'];
    size.update(key, (val) => val = value + 1);
    cloth.data['size'] = size;
    setState(() {
      isChanged = true;
      makeSize();
    });
  }

  decrement(key, value) {
    Map size = cloth.data['size'];
    if (value != 0) {
      size.update(key, (val) => val = value - 1);
      cloth.data['size'] = size;
      setState(() {
        isChanged = true;
        makeSize();
      });
    } else
      print('out of stock');
  }

  changeState() {

    DocumentReference documentReference = updateRef.document(currentUser.sid);
    documentReference.get().then((doc) {
      if (doc.data['frequencyPoint'] != 0) {
        var latestPoint = (new DateTime.now().millisecondsSinceEpoch.toInt()) -
            doc.data['frequencyPoint'] / 2;
        documentReference.updateData({"frequencyPoint": latestPoint});
      }
      else {
        documentReference.updateData({
          "frequencyPoint": new DateTime.now().millisecondsSinceEpoch.toInt()
        });
      }
    });
  }

  saveDb() {
    db.runTransaction((transaction) async {
      DocumentSnapshot freshSnap = await transaction.get(cloth.reference);
      await transaction
          .update(freshSnap.reference, {'size': cloth.data['size']});
      setState(() {
        isChanged = false;
      });
      changeState();
    });
  }
  @override
  Widget build(BuildContext context) {
    data = cloth.data;
    picUrl = data['picUrl'];
    Size screenSize = MediaQuery.of(context).size;

    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            cloth.data['brand'],
            style: new TextStyle(letterSpacing: 2.0),
          ),
          actions: <Widget>[
            new Card(
              shape: new CircleBorder(),
              elevation: 5.0,
              color: Colors.blue,
              child: new Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Center(
                  child: new Text(
                    data['crn'].toString(),
                    style: new TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                        fontWeight: FontWeight.w900),
                  ),
                ),
              ),
            ),
            Center(
              child: new Container(
                margin: const EdgeInsets.only(left: 20.0, right: 5.0),
                child: new Text(
                  data['price'].toString() + ' INR',
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w900),
                ),
              ),
            )
          ],
        ),
        body: new ListView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          children: <Widget>[
            new Container(
              height: 400.0,
              child: new ListView(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  children: getPics()),
            ),
            new Card(
              child: new Column(
                children: <Widget>[
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        child: new Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: new Text(
                            data['type'],
                            style: new TextStyle(
                                fontSize: 18.0, fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                      new Container(
                        child: new Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: new Text(
                            data['gender'] + '  -  ' + data['typeOfWear'],
                            style: new TextStyle(
                                fontSize: 18.0, fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                    ],
                  ),
                  new SizedBox(
                    height: screenSize.height * 0.02,
                  ),
                  isChanged == true ? Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Text('Save the changes',
                          softWrap: true),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: new RaisedButton(
                          onPressed: () => saveDb(),
                          child: new Text('Save',
                            style: new TextStyle(
                                color: Colors.white
                            ),
                          ),
                          color: Colors.blue,

                        ),
                      ),

                    ],
                  ) : new Container(),
                  new SizedBox(
                    height: screenSize.height * 0.02,
                  ),
                  new Container(
                    height: 200.0,
                    child: new ListView(
                      scrollDirection: Axis.horizontal,
                      children: makeSize(),
                    ),
                  ),


                ],
              ),
            ),
          ],
        ));
  }
}
