import 'dart:async';

import 'package:clothx_stores/model/models.dart';
import 'package:clothx_stores/pages/HomePage.dart';
import 'package:clothx_stores/services/ValidationService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ReadyClothFormWidget extends StatefulWidget {
  @override
  ReadyClothFormWidgetState createState() => ReadyClothFormWidgetState();
}

class ReadyClothFormWidgetState extends State<ReadyClothFormWidget> {
  final GlobalKey<FormState> _readyClothAddingFormKey =
  new GlobalKey<FormState>();
  CollectionReference coRef;
  String path;
  bool isUploading = false;
  ReadyMade readyMade;
  int _gender = 0;
  var picUrl = [];
  var picListLength = 0;
  var options = ['Men', 'Women', 'Boys', 'Girls'];
  var size = ['S', 'M', 'L', 'XL', '2XL', '3XL'];
  Validations _validations = new Validations();
  bool _autovalidate = false;
  Auth currentUser;

  @override
  void initState() {
    super.initState();
    currentUser = MyHomeState.sharedData;
    readyMade = ReadyMade(
        "",
        "",
        "",
        {'S': 0,
          'M': 0,
          'L': 0,
          'XL': 0,
          '2XL': 0,
          '3XL': 0},
        0,
        []);
    final firestore = Firestore.instance;
    coRef = firestore.collection('stores/' + currentUser.sid + '/clothes');
  }

  void onChanged(int value) {
    setState(() {
      _gender = value;
    });

    print(options[value]);
  }

  Future<Null> _errorDialog(error) {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('$error'),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  List<Widget> makeRadios() {
    List<Widget> list = new List<Widget>();

    for (int i = 0; i < 4; i++) {
      list.add(
        new Padding(
          padding: const EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              new Text(options[i]),
              new Radio(
                  value: i,
                  groupValue: _gender,
                  onChanged: (int value) {
                    onChanged(value);
                  }),
            ],
          ),
        ),
      );
    }
    return list;
  }

  List<Widget> makeSize() {
    List<Widget> list = new List<Widget>();

    readyMade.size.forEach((k, v) {
      list.add(
        new Container(
          width: 75.0,
          child: new Card(
            elevation: 5.0,
            child: new TextFormField(
              onSaved: (text) {
                var al = readyMade.size.update(
                    k, (val) => val = text.isNotEmpty ? int.parse(text) : 0);
                print(al);
              },
              keyboardType: TextInputType.number,
              decoration: new InputDecoration(
                border: InputBorder.none,
                labelText: k,
                fillColor: Colors.white,
                filled: true,
              ),
              style: new TextStyle(color: Colors.blueGrey[800], fontSize: 20.0),
            ),
          ),
        ),
      );
    });

    return list;
  }

  void handleSave() {
    final FormState form = _readyClothAddingFormKey.currentState;

    if (!form.validate()) {
      _autovalidate = true;
    } else if (picUrl.isNotEmpty) {
      readyMade.gender = options[_gender];
      readyMade.picUrl = picUrl;
      form.save();
      coRef.add(readyMade.toJson());
      _errorDialog('Sucessfully added!');
      picUrl = [];
      setState(() {
        picListLength = picUrl.length;
      });
    } else
      _errorDialog('Atleast minimum a picture');
  }

  Future<Null> _handleAddPhoto(source) async {
    if (picUrl.length <= 10) {
      var random = new DateTime.now().millisecondsSinceEpoch;
      var imageFile = await ImagePicker.pickImage(
          source: source, maxWidth: 1280.0, maxHeight: 1280.0);

      if (imageFile != null) {
        setState(() {
          isUploading = true;
        });
      }

      var ref =
      FirebaseStorage.instance.ref().child(
          'stores/' + currentUser.sid + '/clothes/cloth_$random.jpg');
      var uploadTask = ref.putFile(imageFile);
      var downloadUrl = (await uploadTask.future).downloadUrl;
      picUrl.add(downloadUrl.toString());
      setState(() {
        isUploading = false;
        picListLength = picUrl.length;
      });
    } else
      print('upload limit exceeds');
  }

  reset() {
    final FormState form = _readyClothAddingFormKey.currentState;
    form.reset();
    picUrl = [];
    readyMade.picUrl = picUrl;
    setState(() {
      picListLength = picUrl.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        new Container(
          color: Colors.blueGrey[800],
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: new Text(
                  'reset',
                  style: new TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0,
                      color: Colors.white),
                ),
              ),
              new Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Card(
                      shape: new CircleBorder(),
                      elevation: 5.0,
                      child: new IconButton(
                          icon: Icon(Icons.refresh),
                          onPressed: () => reset()))),
            ],
          ),
        ),
        new Card(
          child: new Form(
              key: _readyClothAddingFormKey,
              autovalidate: _autovalidate,
              child: new Column(children: <Widget>[
                new Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: new Column(
                    children: <Widget>[
                      new TextFormField(
                        validator: _validations.validateText,
                        initialValue: "",
                        onSaved: (val) => readyMade.brand = val,
                        decoration: new InputDecoration(
                          border: InputBorder.none,
                          labelText: "Brand",
                          fillColor: Colors.white,
                          filled: true,
                        ),
                        style: new TextStyle(
                            color: Colors.blueGrey[800], fontSize: 16.0),
                      ),
                      new Divider(
                        color: Colors.black,
                      ),
                      new TextFormField(
                        validator: _validations.validateText,
                        initialValue: "",
                        onSaved: (val) => readyMade.typeOfWear = val,
                        decoration: new InputDecoration(
                          border: InputBorder.none,
                          labelText: "Type of Cloth ",
                          fillColor: Colors.white,
                          filled: true,
                        ),
                        style: new TextStyle(
                            color: Colors.blueGrey[800], fontSize: 16.0),
                      ),
                      new Divider(
                        color: Colors.black,
                      ),
                      new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: makeRadios()),
                      new Divider(
                        color: Colors.black,
                      ),
                    ],
                  ),
                ),
                new Container(
                  margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                  width: double.infinity,
                  height: 75.0,
                  child: new ListView(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      children: makeSize()),
                ),
                new Divider(
                  color: Colors.black,
                ),
                new TextFormField(
                  validator: _validations.validatePrice,
                  initialValue: "",
                  onSaved: (val) => readyMade.price = int.parse(val),
                  keyboardType: TextInputType.number,
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    labelText: "Price",
                    fillColor: Colors.white,
                    filled: true,
                  ),
                  style: new TextStyle(
                      color: Colors.blueGrey[800], fontSize: 16.0),
                ),
                new Divider(
                  color: Colors.black,
                ),
                new Container(
                  margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
                  child: new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Row(
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                isUploading == true
                                    ? new Container(
                                  margin:
                                  const EdgeInsets.only(bottom: 10.0),
                                  child: Row(
                                    children: <Widget>[
                                      new Container(
                                          width: 20.0,
                                          height: 20.0,
                                          child:
                                          new CircularProgressIndicator()),
                                      Padding(
                                        padding:
                                        const EdgeInsets.all(10.0),
                                        child: new Text("Uploading.."),
                                      )
                                    ],
                                  ),
                                )
                                    : new Text('$picListLength pictures added'),
                                Row(
                                  children: <Widget>[
                                    new Card(
                                      elevation: 5.0,
                                      margin: const EdgeInsets.all(10.0),
                                      shape: const CircleBorder(),
                                      child: new IconButton(
                                          icon: new Icon(Icons.photo_camera),
                                          onPressed: () =>
                                              _handleAddPhoto(
                                                  ImageSource.camera)),
                                    ),
                                    new Card(
                                      elevation: 5.0,
                                      margin: const EdgeInsets.all(10.0),
                                      shape: const CircleBorder(),
                                      child: new IconButton(
                                          icon: new Icon(Icons.photo),
                                          onPressed: () =>
                                              _handleAddPhoto(
                                                  ImageSource.gallery)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                        new RaisedButton(
                          onPressed: () => handleSave(),
                          child: new Text('Save'),
                        ),
                      ],
                    ),
                  ),
                )
              ])),
        ),
      ],
    );
  }
}
